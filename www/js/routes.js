angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    function CheckForAuthenticatedUser(ParseHttpService, $state) {
      return ParseHttpService.getCurrentUser().then(function (_user) {
        // if resolved successfully return a user object that will set
        // the variable `resolvedUser`
        return _user;
      }, function (_error) {
        $state.go('login');
      })
    }

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      })


      .state('signup', {
        url: '/signup',
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      })


      .state('tabsController', {
        url: '/tabscontroller',
        abstract: true,
        templateUrl: 'templates/tabsController.html',
        resolve: {
          resolvedUser: CheckForAuthenticatedUser
        }
      })


      .state('tabsController.trending', {
        url: '/trending',
        views: {
          'tab4': {
            templateUrl: 'templates/trending.html',
            controller: 'trendingCtrl'
          }
        },
        resolve: {
          CurrentUser: function (resolvedUser) {
            return resolvedUser;
          }
        }
      })


      .state('tabsController.browse', {
        url: '/browse',
        views: {
          'tab6': {
            templateUrl: 'templates/browse.html',
            controller: 'browseCtrl'
          }
        },
        resolve: {
          CurrentUser: function (resolvedUser) {
            return resolvedUser;
          }
        }

      })


      .state('tabsController.profile', {
        url: '/profile',
        views: {
          'tab7': {
            templateUrl: 'templates/profile.html',
            controller: 'profileCtrl'
          }
        },
        resolve: {
          CurrentUser: function (resolvedUser) {
            return resolvedUser;
          }
        }
      })


      .state('facebookSignupPage', {
        url: '/facebook',
        templateUrl: 'templates/facebookSignupPage.html',
        controller: 'facebookSignupPageCtrl'
      })


      .state('myAccount', {
        url: '/myaccount',
        templateUrl: 'templates/myAccount.html',

        controller: 'myAccountCtrl',
        resolve: {
          resolvedUser: CheckForAuthenticatedUser,
          CurrentUser: function (resolvedUser) {
            return resolvedUser;
          }
        }



      })

      .state('password', {
        url: '/password',
        templateUrl: 'templates/password.html',
        controller: 'passwordCtrl'
      })


      .state('resetPassword', {
        url: '/reset',
        templateUrl: 'templates/resetPassword.html',
        controller: 'resetPasswordCtrl'
      })


      .state('listItemPage', {
        url: '/listitempage/:item',
        templateUrl: 'templates/listItemPage.html',
        controller: 'listItemPageCtrl',
        resolve: {
          resolvedUser: CheckForAuthenticatedUser,
          CurrentUser: function (resolvedUser) {
            return resolvedUser;
          }
        }

      })


      .state('subCategoryPage', {
        url: '/subCategory/:category:objectId',
        templateUrl: 'templates/subCategoryPage.html',
        controller: 'subCategoryPageCtrl'

      })
      .state('itemDescription', {
        url: '/itemdescription/:name',
        templateUrl: 'templates/itemDescription.html',
        controller: 'itemDescriptionCtrl'
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

  });
