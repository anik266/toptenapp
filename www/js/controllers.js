angular.module('app.controllers', ['app.services'])

  .controller('loginCtrl', function ($scope, $state, ParseHttpService, $timeout) {
    console.log("In Login Controller");

    $scope.credentials = {};

    $scope.doLoginAction = function () {
      console.log($scope.credentials);
      ParseHttpService.login($scope.credentials).then(function (_user) {
        $timeout(function () {

          $state.go('tabsController.trending', {});
          console.log("user", _user);

        }, 2);

      }, function (_error) {
        alert("Login Error " + (_error.message ? _error.message : _error.data.error));
        $state.go('login', {});
      });
    }
  })

  .controller('signupCtrl', function ($scope, ParseHttpService) {
    console.log("In signupctrl");
    $scope.signupid = {
      username: "",
      password: "",
      email: ""
    };
    $scope.doSignup = function () {
      console.log($scope.signupid);
      ParseHttpService.Signup($scope.signupid).then(function (response) {
        alert("Congratulations, you are signed up for Top10!");
      }, function (_error) {
        alert("Login Error " + (_error.message ? _error.message : _error.data.error));
      });
        $scope.signupid = {
          username: "",
          password: "",
          email: ""
        };
    }
  })

  .controller('browseCtrl', function ($scope, $state, ParseHttpService) {
    //$scope.BookList = ParseHttpService.getBooksByGenre("Fiction");
    $scope.stateInfo = $state.current;
    $scope.params = $state.params;
    console.log($scope.stateInfo);
    $scope.CategoryList = ["Books","Movies","Recipies","Music","Holiday Gifts"];
  })

  .controller('trendingCtrl', function ($scope, $state, ParseHttpService, CurrentUser) {

    ParseHttpService.getMovie("Red Eye", CurrentUser);

    $scope.BookList = [];
    ParseHttpService.getBooksFrom("Books").then(function (response) {
      $scope.BookList = response.data.results;
      ParseHttpService.getBooksFrom("Movies").then(function (response) {
        $scope.BookList = $scope.BookList.concat(response.data.results);
        //console.log("lollll", $state.url);
        //return initializeController();
      });
      //console.log("lollll", $state.url);
      //return initializeController();
    });
    $scope.user = CurrentUser;
    //Trending page upvote-downvote functionality
    $scope.upVote = function (Item, action, genre) {
      console.log($scope.user.username, "clicked on ", Item, action);
      ParseHttpService.updateItem(Item, action, genre);      //UPVOTING
      return populate();
    };
    $scope.downVote = function (Item, action,genre) {
      console.log($scope.user.username, "clicked on ", Item, action);
      ParseHttpService.updateItem(Item, action, genre);      //DOWNVOTING
      return populate();
    };
    function populate() {

      return ParseHttpService.getBooksFrom("Books").then(function (response) {
        $scope.BookList = response.data.results;
        ParseHttpService.getBooksFrom("Movies").then(function (response) {
          $scope.BookList = $scope.BookList.concat(response.data.results);
          //console.log("lollll", $state.url);
          //return initializeController();
        });
        //console.log("lollll", $state.url);
        //return initializeController();
      });
        //console.log("accessed: ", $scope.contributionList);

      }


    $scope.initializeController = function () {
      return populate();
    }
  })

  .controller('profileCtrl', function ($scope, $state, ParseHttpService, $timeout, CurrentUser) {
    initializeController();
    console.log("In profileCtrl");
    $scope.contributionList = [];
    $scope.username = CurrentUser.username;
    $scope.inputItem = {
      title: "",
      author: ""
    };
    $scope.inputItem2 = {
      title: "",
      author: ""
    };
    //console.log($scope.inputItem);
    console.log(CurrentUser);
    $scope.addNewBook = function () {
      console.log("Clicked on Add<<Profile: " + $scope.inputItem.title);
      ParseHttpService.getBook($scope.inputItem.title, CurrentUser).then(function (response1) {
        console.log(response1);
        $scope.inputItem = {};
         // ParseHttpService(Item,bookTitle);
      }, function (_error) {
        alert("Login Error " + (_error.message ? _error.message : _error.data.error));
      });

    };

    $scope.addNewMovie = function () {
      var newVariable = $scope.inputItem2.title;
      $scope.inputItem2 = {};
      console.log("Clicked on Add<<Profile: " + $scope.inputItem2.title);
      ParseHttpService.getMovie(newVariable, CurrentUser).then(function (response) {
        console.log(response);

        // ParseHttpService(Item,bookTitle);
      }, function (_error) {
        alert("Login Error " + (_error.message ? _error.message : _error.data.error));
      });
    };
    //return initializeController();

    //$scope.addNewBook();
    //Displays the user's contribution
    //initializeController();
    //populateList();
    console.log(CurrentUser.username);

    ParseHttpService.getBooksByUser(CurrentUser).then(function (response) {
      //$timeout(function () {
        //$scope.contributionList = response;
        var buildingList = response;
        console.log(buildingList);
        ParseHttpService.getMoviesByUser(CurrentUser).then(function(response2){
          var buildingList2 = buildingList.concat(response2);
          $scope.contributionList = buildingList2;
          console.log($scope.contributionList);
        })
      }, function (_error) {
      alert("Login Error " + (_error.message ? _error.message : _error.data.error));
    });
    //logout function
    $scope.doLogoutAction = function doLogoutAction() {
      console.log("Signing out");
      $scope.contributionList = [];
      ParseHttpService.logout().then(function (_response) {
        $timeout(function () {
          $state.go('login', {});
          console.log("logout: logout", _response);
        }, 2);
      })
    };

    function populateList() {
      return ParseHttpService.getBooksByUser(CurrentUser).then(function (response) {
        $scope.contributionList = response;
        console.log("accessed: ", $scope.contributionList);
      });
    }

    function initializeController() {
      console.log("In initialize");
      return populateList();
    }

    $scope.initializeController = function () {
      return populateList();
    };

  })

  .controller('facebookSignupPageCtrl', function ($scope, $state, $cordovaFacebook) {
    console.log("in fbsignupctrl");
    $scope.userData = {
      username: "",
      password: ""
    };

    $scope.doLogin = function () {
      console.log("in doLoagin");

      var fbLoginSuccess = function (userData) {
        alert("UserInfo: " + JSON.stringify(userData));
      };
      console.log($scope.userData);
      $cordovaFacebook.login(["public_profile"],
        fbLoginSuccess,
        function (error) {
          alert("" + error)
        }
      );
    }
  })

  .controller('myAccountCtrl', function ($scope,$state, CurrentUser) {
      $scope.user = CurrentUser;
      console.log($scope.user);

  })
  .controller('passwordCtrl', function ($scope) {

  })

  .controller('resetPasswordCtrl', function ($scope) {

  })

  .controller('listItemPageCtrl', function ($scope, $state, ParseHttpService,CurrentUser) {
    $scope.stateInfo = $state.current;
    $scope.params = $state.params;
    console.log($scope.params);
    console.log("hello");

    console.log($scope.stateInfo);
    console.log($state.params);
    $scope.Back = function () {
      $state.go('subCategoryPage', {});
    };
    //  $scope.user = CurrentUser;
    $scope.upVote = function (Item, action) {
      //  console.log($scope.user.username, "clicked on ", Item, action);
      ParseHttpService.updateItem(Item, action);      //UPVOTING
      // return populate();
    };
    $scope.downVote = function (Item, action) {
      // console.log($scope.user.username, "clicked on ", Item, action);
      ParseHttpService.updateItem(Item, action);      //DOWNVOTING
      //return populate();
    };
    $scope.GenreList = [];
    ParseHttpService.getBooksByGenre($state.params.item).then(function (response) {
      $scope.GenreList = response;
  //    ParseHttpService.updateBookImage($scope.params, $scope.params.name).then(function (response){
    //    console.log("add image function response",response);

     // })
    })
  })


  .controller('subCategoryPageCtrl', function ($scope, $state) {
    $scope.params = $state.params;
    console.log("category",$scope.params);
    if ($scope.params.category == "Books") {
      $scope.genreList = ["Fiction", "Non-fiction", "Self-help", "Biography", "Miscelleneous"];
    }
    else {
      $scope.genreList = ["Drama", "Action", "Horror", "Comedy", "Others"];
    }
    $scope.stateInfo = $state.current;
    $scope.params = $state.params;

    console.log($scope.params);

    $scope.Back = function () {
      $state.go('tabsController.browse', {});
    };
    console.log();
    //$scope.clicked = "Fiction";
  })

  .controller('itemDescriptionCtrl', function ($scope, $state, ParseHttpService) {
    $scope.params = $state.params;
    $scope.stateInfo = $state.current;
    console.log($scope.params);

    //$scope.info = "123";
    $scope.critic = [];


    console.log("item description", $scope.params.name);

    ParseHttpService.fetchMovie($scope.params.name).then(function (response) {
      console.log(response);
      if (response.data.Type == "movie") {
        $scope.genre = response.data.Genre;
        $scope.rating = response.data.imdbRating;
        $scope.length = response.data.Runtime;
        $scope.release_date = response.data.Released;
        // var info = $scope.info;
        var movieCritic = {source:"IMDB", snippet:response.data.tomatoConsensus, star_rating:response.data.tomatoUserRating, review_date:response.data.DVD};
        $scope.critic = [movieCritic];
        $scope.imageLink = response.data.Poster;
        $scope.author = response.data.Director;
        $scope.title = response.data.Title;
        $scope.sub_title = "";

      }
      else {
        ParseHttpService.fetchBook($scope.params.name).then(function (response) {
          console.log(response);

          $scope.genre = response.data.book.genre;
          $scope.rating = response.data.book.rating;
          $scope.length = response.data.book.length;
          $scope.release_date = response.data.book.release_date;
          // var info = $scope.info;
          $scope.critic = response.data.book.critic_reviews;
          $scope.author = response.data.book.author;
          $scope.title = response.data.book.title;
          $scope.sub_title = response.data.book.sub_title;


        });

        ParseHttpService.fetchBookFromParse($scope.params.name).then(function (response) {
          ParseHttpService.updateBookImage(response, $scope.params.name).then(function (response2){

            console.log("add image function response",response2);
            $scope.imageLink = response.cover;



            console.log($scope.imageLink);//UPVOTING
          })
        });

      }
    });


    $scope.upVote = function (action) {
      ParseHttpService.fetchBookFromParse($scope.params.name).then(function (response) {
        if (response != "Not found") {
          ParseHttpService.updateItem(response, action, response.genre);      //UPVOTING
        }
        else {
          ParseHttpService.fetchMovieFromParse($scope.params.name).then(function(response){
            ParseHttpService.updateItem(response,action,response.genre.split(",")[0]);
        });
        }

      })
    };
    $scope.downVote = function (action) {
      ParseHttpService.fetchBookFromParse($scope.params.name).then(function (response) {
        console.log(response);
        if (response != "Not found") {
          ParseHttpService.updateItem(response, action, response.genre);      //UPVOTING
        }
        else {
          ParseHttpService.fetchMovieFromParse($scope.params.name).then(function(response){
            console.log(response);
            ParseHttpService.updateItem(response,action,response.genre.split(",")[0]);
          });
        }     //DOWNVOTING
      })
    };
  });
