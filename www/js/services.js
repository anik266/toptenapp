angular.module('app.services', [])

  .factory('BlankFactory', [function () {

  }])
  .service('UserService', [
    '$q', 'ParseConfiguration', '$cordovaFacebook',
    function ($q, ParseConfiguration, $cordovaFacebook) {

      return {
        doParseLoginWithFB: doParseLoginWithFB
      };

      function doParseLoginWithFB() {
        try {
          // STEP 1 - LOGIN TO FACEBOOK
          return $cordovaFacebook.login(["public_profile", "email", "user_friends"])
            .then(function (success) {
              // save access_token
              var accessToken = success.authResponse.accessToken;
              var userID = success.authResponse.userID;
              var expiresIn = success.authResponse.expiresIn;

              console.log("Login Success" + JSON.stringify(success));

              // STEP - 2 CONVERTING DATE FORMAT
              var expDate = new Date(
                new Date().getTime() + expiresIn * 1000
              ).toISOString();

              function doParseLogin(authData) {
                Parse.initialize(ParseConfiguration.applicationId, ParseConfiguration.javascriptKey);
                return Parse.FacebookUtils.logIn(authData);
              }

              // STEP - 3 LOGIN TO PARSE
              return doParseLogin({
                id: userID,
                access_token: accessToken,
                expiration_date: expDate
              });
            }).then(function (_parseResult) {

              // STEP - 4 GET ADDITIONAL USER INFORMATION FROM FACEBOOK
              // get the user information to add to the Parse Object
              var fbValues = "&fields=id,name,location,website,picture.width(100).height(100),email";
              var fbPermission = ["public_profile"];

              return $cordovaFacebook.api("me?access_token=" + accessToken + fbValues, fbPermission);
            }).then(function (_fbUserInfo) {

              // use the information to update the object
              // STEP - 5 UPDATE THE USER OBJECT
              var username = _fbUserInfo.name.toLocaleLowerCase().replace(" ", "");
              var email = _fbUserInfo.email;
              var profileImg = _fbUserInfo.picture.data.url;

              // add the username, email and profileImg to the parse object
              Parse.User.current().set("username", username);
              Parse.User.current().set("email", email);
              Parse.User.current().set("profileImage", profileImg);

              console.log(username);
              console.log(profileImg);


              return Parse.User.current().save();
            }).then(function (_updatedUser) {

              return _updatedUser;
            })
        } catch (_error) {
          return $q.reject("Missing Facebook Plugin - This functionality only works on device");
        }
      }


    }]
)


  .service('ParseHttpService', function ($http, $q) {
    //PRIVATE function
    function updateUser(CurrentUser, column, id) {
      //column is either "upvoted" or "downvoted"
      //id is the items's id that was upvoted or downvoted
      console.log("token: " + CurrentUser.sessionToken + "id: " + id);

      var settings = {
        headers: authenticationHeaders,
        'X-Parse-Session-Token': CurrentUser.sessionToken,
        method: 'PUT',
        'Content-Type': 'application/json'
      };
      if (column == "upvoted") {
        var dataObject = {"upvoted": [id]};
      }
      if (column == "downvoted") {
        var dataObject = {"downvoted": [id]};
      }
      var dataObjectString = JSON.stringify(dataObject);
      console.log(dataObjectString);
      var Url = 'https://api.parse.com/1/users/' + CurrentUser.objectId;
      return $http.put(Url, dataObjectString, settings)
        .then(function (response) {
          // In the response resp.data contains the result
          // check the console to see all of the data returned
          console.log('updated User columns: ' + response);
          return response.data;
        });
    }
    var imageLinkObject = {imageLink:""};

    //adds book to Parse /1/classes/Books
    function addBookToParse(response, CurrentUser, imageLinkObject) {
      if (response.data.total_results = 0) {
        //console.log("Book not stored");
        return;
      }

      var NewBook = {
        "name": response.data.book.title,
        "author": response.data.book.author,
        "genre": response.data.book.genre,
        "pages": response.data.book.pages,
        "upvoters": [CurrentUser.username],
        "downvoters": [],
        "promoted": 1,
        "rating": response.data.book.rating,
        "upvote": 1,
        "downvote": 0,
        "creator": [CurrentUser.username],      //username who is logged in
        "cover": imageLinkObject.imageLink
      };
      NewBook.promoted = ((NewBook.upvoters.length - NewBook.downvoters.length));


      var settings = {
        async: true,
        crossDomain: true,
        headers: authenticationHeaders,
        url: "https://api.parse.com/1/classes/Books",
        method: "POST",
        data: JSON.stringify(NewBook)
      };

      var pullSettings = {
        method: 'GET',
        url: baseURL + 'classes/Books',
        headers: authenticationHeaders
      };
      $http(pullSettings)
        .then(function (response2) {
          console.log('Books already in Parse', response2);
          //return response;
          var matchFound = false;
          var matchCount = 0;
          var tempBookList = response2.data.results;
          for (var i = 0; i < (tempBookList.length); i++) {
            if (tempBookList[i].name == response.data.book.title) {
              //ParseBookList.push(tempBookList[i]);
              matchFound = true;
              var matchBookId = tempBookList[i].objectId;
              var arraynumber = i;
              matchCount += 1;
              tempBookList[i].creator.push(CurrentUser.username);
              if (tempBookList[i].upvoters.indexOf(CurrentUser.username) < 0) {
                tempBookList[i].upvoters.push(CurrentUser.username);
              }
            }
          }
          if (matchFound) {
            //append username to creator[] and upvoters[] arrays
            //var newupvoters = response2.data.results.upvoters.push(CurrentUser.username);
            //var newcreator = response2.data.results.creator.push(CurrentUser.username);
            var pushSettings = {
              headers: authenticationHeaders
            };
            var pushurl = baseURL + 'classes/Books/' + matchBookId;
            var new_info = {
              upvoters: tempBookList[arraynumber].upvoters,
              //add creator array here
              creator: tempBookList[arraynumber].creator
            };
            console.log("new_info", new_info);
            var pushdataObjectString = JSON.stringify(new_info);

            return $http.put(pushurl, pushdataObjectString, settings)
              .then(function (response) {
                console.log("match found", matchFound, matchCount, "times", matchBookId);
                console.log("new book list", tempBookList);
              });
          }
          else {
            return $http(settings)
              .then(function (response7) {
                console.log("added book to parse: ", response7);
                //console.log(response.data.objectId);

                //store object's id to user's upvoted array.
                //updateUser(CurrentUser, "upvoted", response.data.objectId);

                return response7;
              }, function error(_errorResponse) {
                alert("ERROR " + _errorResponse);
              });
          }
        });

    }

    var baseURL = "https://api.parse.com/1/";
    var authenticationHeaders = {
      "x-parse-application-id": "FqFFwmhl5KHJBGxLgkGBgpHUzdQl0WEphcWJvLTT",
      "x-parse-rest-api-key": "94xXoB56nBWPBFApR6T31pRIpXbLMcaA9HglBzLF"
    };

    var CurrentUser = null;
    var CurrentUserIsRestored = false;

    function restoreUser(_sessionToken) {

      var tempHeaders = angular.copy(authenticationHeaders);
      angular.extend(tempHeaders, {"X-Parse-Session-Token": _sessionToken});

      var settings = {
        headers: tempHeaders
      };


      // $http returns a promise, which has a then function,
      // which also returns a promise
      return $http.get(baseURL + 'users/me', settings)
        .then(function (response) {
          // In the response resp.data contains the result
          // check the console to see all of the data returned
          console.log('restoreUser', response);

          CurrentUserIsRestored = true;

          return response.data;
        }, function (_error) {
          window.localStorage.setItem('CurrentUser', null);
          CurrentUserIsRestored = false;
          CurrentUser = null;
          console.log('restoreUser', _error);
          $q.reject("NO USER");
        });
    }

    // these are functions exposed to public
    return {
      getCurrentUser: function () {
        CurrentUser = window.localStorage.getItem('CurrentUser');
        CurrentUser = CurrentUser && JSON.parse(CurrentUser);

        if (CurrentUser) {
          if (CurrentUserIsRestored) {
            return $q.when(CurrentUser)
          }
          return restoreUser(CurrentUser.sessionToken);

        } else {
          return $q.reject("NO USER");
        }
      },


      /**
       * default function for logging in user
       * @param credentials
       * @returns {*}
       */
      login: function (credentials) {
        var settings = {
          headers: authenticationHeaders,
          params: {
            "username": (credentials && credentials.username),
            "password": (credentials && credentials.password)
          }
        };
        return $http.get(baseURL + 'login', settings)
          .then(function (response) {
            // NEW CODE - save the current user - NEW CODE !!!
            CurrentUser = response.data;
            window.localStorage.setItem('CurrentUser', JSON.stringify(CurrentUser));
            return response.data;
          });
      },

      Signup: function (credentials) {
        var settings = {
          async: 'true',
          crossDomain: 'true',
          headers: authenticationHeaders,
          method: 'POST',
          'X-Parse-Revocable-Session': 1,
          'Content-Type': 'application/json'
        };
        //    var url = "https://api.parse.com/1/users";
        credentials.upvoted = [];
        credentials.downvoted = [];
        var dataObject = JSON.stringify(credentials);

        console.log(dataObject);
        return $http.post(baseURL + 'users', dataObject, settings)
          .then(function (response) {
            // In the response resp.data contains the result
            // check the console to see all of the data returned
            console.log('addObject', response);
            return response.data;
          });
      },

      logout: function () {
        var tempHeaders = angular.copy(authenticationHeaders);
        angular.extend(tempHeaders, {"X-Parse-Session-Token": CurrentUser.sessionToken});
        var settings = {
          headers: tempHeaders
        };
        return $http.post(baseURL + 'logout', {}, settings)
          .then(function (response) {
            console.log('logout', response);
            window.localStorage.setItem('CurrentUser', null);
            return response;
          }, function (_error) {
            console.log(_error);
            window.localStorage.setItem('CurrentUser', null);
          });
      },

      //Returns all books contributed by the user
      getBooksByUser: function (user) {
        var contributionList = [];
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/Books/',
          headers: authenticationHeaders
        };
        return $http(settings)
          .then(function (response3) {
            //code to filter books based by creator
            var tempList = response3.data.results;
            console.log('response 3', response3);
            console.log("Parse booke", tempList);
            for (var i = 0; i < (tempList.length); i++) {
              if (tempList[i].creator.indexOf(user.username) > -1) {//(tempList[i].creator == user.username){
                contributionList.push(tempList[i]);
              }
            }
            console.log("user contributed: ", contributionList);
            return contributionList;
          });
      },
      //returns a list of movies added by user
      getMoviesByUser: function (user) {
        var contributionList = [];
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/Movies/',
          headers: authenticationHeaders
        };
        return $http(settings)
          .then(function (response3) {
            //code to filter books based by creator
            var tempList = response3.data.results;
            //console.log('response 3', response3);
            console.log("Parse movies", tempList);
            for (var i = 0; i < (tempList.length); i++) {
              if (tempList[i].creator.indexOf(user.username) > -1) {//(tempList[i].creator == user.username){
                contributionList.push(tempList[i]);
              }
            }
            console.log("user contributed: ", contributionList);
            return contributionList;
          });
      },
      //gets stuff from any table passed as parameter.
      getBooksFrom: function (table) {
        // table = classes/<name of table>
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/' + table,
          headers: authenticationHeaders
        };
        // $http returns a promise, which has a then function,
        // which also returns a promise
        return $http(settings)
          .then(function (response) {
            // In the response resp.data contains the result
            // check the console to see all of the data returned
            return response;
          }, function error(_errorResponse) {
            // if error occurred anywhere above, this code will
            // be executed
            alert("ERROR " + _errorResponse);
          });
      },
      //returns an array of desired genre of books from Parse.
      getBooksByGenre: function (genre) {
        var bookList = [];

        var subCategoryList1 = ["Fiction", "Non-fiction", "Self-help", "Biography", "Miscelleneous"];
        if (subCategoryList1.indexOf(genre) > -1) {
          var urlpostfix = "Books";
        }

        var subCategoryList2 = ["Drama", "Action", "Horror", "Comedy", "Others"];
        if (subCategoryList2.indexOf(genre) > -1) {
          var urlpostfix = "Movies";
        }
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/' + urlpostfix,
          headers: authenticationHeaders
        };
        return $http(settings)
          .then(function (response) {
            console.log('getStuff', response);
            //return response;
            var tempBookList = response.data.results;
            for (var i = 0; i < (tempBookList.length); i++) {
              if (tempBookList[i].genre == genre) {
                bookList.push(tempBookList[i]);
              }
            }
            return bookList;
          }, function error(_errorResponse) {
            alert("ERROR " + _errorResponse);
          });
      },
      //Gets book using iDreamBooks API.
      //Input ISBN, Author, Title or EISBN
      getBook: function (booktitle, CurrentUser) {
        //console.log("user" + CurrentUser.username);
        var _id = booktitle; //prompt("Enter Book's Title, ISBN, Author or EISBN");
        if ((_id == '') || (_id == null)) {
          console.log("Not a valid input. Empty string.");
          return;
        }
        key = "6a29d782415bc72c16072d0e2a4592f02753456f";
        var settings = {
          url: "http://idreambooks.com/api/books/reviews.json?q=" + _id + "&key=" + key,
          method: "GET"
        };
        //Goes to Parse, collects books listed by user
        var contributedList = [];
        var settingsParse = {
          method: 'GET',
          url: baseURL + 'classes/Books/',
          headers: authenticationHeaders
        };
        $http(settingsParse)
          .then(function (response) {
            //code to filter books based on creator
            var tempList = response.data.results;
            for (var i = 0; i < (tempList.length); i++) {
              if (tempList[i].creator.indexOf(CurrentUser.username) > -1) {//(tempList[i].creator == CurrentUser.username){
                contributedList.push(tempList[i].name);
              }
            }
            console.log(contributedList);
            console.log(_id);
          });

         return $http(settings)
          .then(function (response) {
            console.log("got book: ", response);
             var settings10 = {
               url: "https://www.googleapis.com/books/v1/volumes?q=" + response.data.book.title,
               method: "GET"
             };


              $http(settings10)
               .then(function (response20) {
                  console.log(response20);
                 console.log("imagelink", response20.data.items[0].volumeInfo.imageLinks.thumbnail);
                 imageLinkObject.imageLink = response20.data.items[0].volumeInfo.imageLinks.thumbnail;


               });
            //checks if the user already added the book previously and throws alert
            if (contributedList.indexOf(response.data.book.title) > -1) {
              alert("You have already added that book.");
            }
            else {
              console.log(imageLinkObject.imageLink);

              addBookToParse(response, CurrentUser, imageLinkObject);

            }
          }, function error(_errorResponse) {
            alert("ERROR " + _errorResponse);
          });
      },

      updateItem: function (Item, action, genre) {
        //action is either "upvoted" or "downvoted"
        //Item (object) is what was upvoted or downvoted
        //CurrentUser is object representing the logged in user
        //console.log("updating item | id: " + Item.objectId + "Item: " + Item.name, action,"by", CurrentUser.username);
        var subCategoryList1 = ["Fiction", "Non-fiction", "Self-help", "Biography", "Miscelleneous"];
        if (subCategoryList1.indexOf(genre) > -1) {
          var Category = "Books";
        }
        else {
          var subCategoryList2 = ["Drama", "Action", "Horror", "Comedy", "Others"];
          if (subCategoryList2.indexOf((genre.split(","))[0]) > -1) {
            var Category = "Movies";
          }
        }



        Array.prototype.remove = function (value) {
          if (this.indexOf(value) !== -1) {
            this.splice(this.indexOf(value), 1);
            return true;
          } else {
            return false;
          }
        };
        var settings = {
          headers: authenticationHeaders,
          method: 'PUT',
          //'X-Parse-Revocable-Session': 1,
          'Content-Type': 'application/json'
        };
        var alreadyup, alreadydown;
        if (Item.upvoters.indexOf(CurrentUser.username) > -1) {
          console.log("already upvoted!");
          alreadyup = true;
        }
        else {
          console.log("Not yet upvoted");
          alreadyup = false;
        }
        if (Item.downvoters.indexOf(CurrentUser.username) > -1) {
          console.log("already downvoted!");
          alreadydown = true;
        }
        else {
          console.log("Not yet downvoted");
          alreadydown = false;
        }
        if ((action == "upvoted") && (!Boolean(alreadyup))) {
          if (Boolean(alreadydown)) {
            Item.downvoters.remove(CurrentUser.username);
          }
          //append user to upvoted
          Item.upvoters.push(CurrentUser.username);
          console.log("removed from dvoted; appended user to upvoted");
          var uplen = Item.upvoters.length;
          var dolen = Item.downvoters.length;
          var promoted = ((uplen - dolen));
          var dataObject = {"upvoters": Item.upvoters, "downvoters": Item.downvoters, "promoted": promoted};
        }
        else if ((action == "downvoted") && (!Boolean(alreadydown))) {
          if (Boolean(alreadyup)) {
            Item.upvoters.remove(CurrentUser.username);
          }
          //append user to downvoted
          Item.downvoters.push(CurrentUser.username);
          console.log("removed from upvoted; appended to downvoted");
          var uplen = Item.upvoters.length;
          var dolen = Item.downvoters.length;
          var promoted = ((uplen - dolen));
          var dataObject = {"downvoters": Item.downvoters, "upvoters": Item.upvoters, "promoted": promoted};
        }
        var dataObjectString = JSON.stringify(dataObject);
        //console.log(dataObjectString);
        var Url = "https://api.parse.com/1/classes/" + Category + "/" + Item.objectId;
        return $http.put(Url, dataObjectString, settings)
          .then(function (response) {
            // In the response resp.data contains the result
            // check the console to see all of the data returned
            console.log(response);
            return response.data;
          });
      },
      fetchBook: function (booktitle) {
        var key = "6a29d782415bc72c16072d0e2a4592f02753456f";
        var settings = {
          url: "http://idreambooks.com/api/books/reviews.json?q=" + booktitle + "&key=" + key,
          method: "GET"
        };
        return $http(settings)
          .then(function (response) {
            return response;
          }, function error(_errorResponse) {
            alert("ERROR " + _errorResponse);
          });
      },

      fetchMovie: function (movietitle) {
        return $http.get("http://www.omdbapi.com/?t=" + movietitle + "&tomatoes=true&plot=full")
          .then(function (response) {
            return response;
          });
      },
      fetchBookFromParse: function (bookTitle) {
        // table = classes/<name of table>
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/Books',
          headers: authenticationHeaders
        };
        // $http returns a promise, which has a then function,
        // which also returns a promise
        return $http(settings)
          .then(function (response) {
            // In the response resp.data contains the result
            // check the console to see all of the data returned
            for (var i = 0; i < response.data.results.length; i++) {
              if (response.data.results[i].name == bookTitle) {
                return response.data.results[i];
              }

            }
            return "Not found";
          }, function error(response) {
            // if error occurred anywhere above, this code will
            // be executed
            return response;

          });
      },
      fetchMovieFromParse: function (bookTitle) {
        // table = classes/<name of table>
        var settings = {
          method: 'GET',
          url: baseURL + 'classes/Movies',
          headers: authenticationHeaders
        };
        // $http returns a promise, which has a then function,
        // which also returns a promise
        return $http(settings)
          .then(function (response) {
            // In the response resp.data contains the result
            // check the console to see all of the data returned
            for (var i = 0; i < response.data.results.length; i++) {
              if (response.data.results[i].name == bookTitle) {
                return response.data.results[i];
              }
            }
          }, function error(_errorResponse) {
            // if error occurred anywhere above, this code will
            // be executed
            alert("ERROR " + _errorResponse);
          });
      },


      getMovie: function (movietitle, CurrentUser) {
        var contributedList = [];
        var getSettings = {
          method: 'GET',
          url: baseURL + 'classes/Movies',
          headers: authenticationHeaders
        };

        var putsettings = {
          headers: authenticationHeaders,
          method: 'PUT',
          //'X-Parse-Revocable-Session': 1,
          'Content-Type': 'application/json'
        };

        $http(getSettings)
          .then(function (response) {
            //code to filter books based on creator
            var tempList = response.data.results;
            for (var i = 0; i < (tempList.length); i++) {
              if (tempList[i].creator.indexOf(CurrentUser.username) > -1) {
                contributedList.push(tempList[i].name);
              }

            }
            console.log("movies contributed by user: ", contributedList);
          });

        $http.get("http://www.omdbapi.com/?t=" + movietitle + "&tomatoes=true&plot=full")
          .then(function (response) {

            if (contributedList.indexOf(response.data.Title) > -1) {
              alert("You have already added that movie.");
            }
            else {
              // Check if the item is already in Parse.
              // If yes, then simply append username to creator[]
              // Also append username to upvoters[] if NOT already, else ignore otherwise
              // If match not found, add the item to Parse


              //console.log("Unique movie input, adding to Parse");
              console.log("Cover of movie: ", response);
              console.log("Cover of movie: ", response.data.Poster);
              //Instantiate new Item
              var NewItem = {
                "name": response.data.Title,
                "author": response.data.Director,
                "genre": (response.data.Genre.split(","))[0],
                //"pages": response.data.pages,
                "upvoters": [CurrentUser.username],
                "downvoters": [],
                "promoted": 1,
                "rating": Number(response.data.imdbRating),
                "upvote": 1,
                "downvote": 0,
                "creator": [CurrentUser.username],    //username who is logged in
                "cover": response.data.Poster
              };
              NewItem.promoted = NewItem.upvoters.length - NewItem.downvoters.length;


              $http(getSettings)
                .then(function (response2) {
                  //console.log('Books already in Parse', response2);

                  var matchFound = false;
                  var matchCount = 0;
                  var tempList = response2.data.results;
                  for (var i = 0; i < (tempList.length); i++) {
                    if (tempList[i].name == response.data.Title) {
                      //ParseBookList.push(tempList[i]);
                      matchFound = true;
                      var matchedId = tempList[i].objectId;
                      var arraynumber = i;
                      matchCount += 1;
                      //tempList[i].creator.push(CurrentUser.username);
                      if (tempList[i].upvoters.indexOf(CurrentUser.username) < 0) {
                        tempList[i].upvoters.push(CurrentUser.username);
                        tempList[i].creator.push(CurrentUser.username);
                      }
                    }
                  }
                  if (matchFound) {
                    //append username to creator[] and upvoters[] arrays
                    //var newupvoters = response2.data.results.upvoters.push(CurrentUser.username);
                    //var newcreator = response2.data.results.creator.push(CurrentUser.username);
                    var pushSettings = {
                      headers: authenticationHeaders
                    };
                    var pushurl = baseURL + 'classes/Movies/' + matchedId;
                    var new_info = {
                      upvoters: tempList[arraynumber].upvoters,
                      //add creator array here
                      creator: tempList[arraynumber].creator
                    };
                    console.log("new_info", new_info);
                    var pushdataObjectString = JSON.stringify(new_info);

                    return $http.put(pushurl, pushdataObjectString, putsettings)
                      .then(function (response) {
                        console.log("match found", matchFound, matchCount, "times", matchedId);
                        console.log("new book list", tempList);
                      });
                  }
                  else {
                    var postsettings = {
                      async: true,
                      crossDomain: true,
                      headers: authenticationHeaders,
                      url: "https://api.parse.com/1/classes/Movies",
                      data: JSON.stringify(NewItem),
                      method: "POST"
                    };
                    console.log(postsettings);
                    return $http(postsettings)
                      .then(function (response) {
                        console.log("added movie to parse: ", response);
                        //console.log(response.data.objectId);

                        //store object's id to user's upvoted array.
                        //updateUser(CurrentUser, "upvoted", response.data.objectId);

                        return response;
                      }, function error(_errorResponse) {
                        console.log("ERROR " + _errorResponse);
                      });
                  }
                });
            }
          }, function error(_errorResponse) {
            alert("ERROR " + _errorResponse);
          });

      },
      updateBookImage: function (Item, bookTitle) {


        var settings = {
          url: "https://www.googleapis.com/books/v1/volumes?q=" + bookTitle,
          method: "GET"
        };
        var imageLink = "";
        return $http(settings)
          .then(function (response) {
            console.log("imagelink", response.data.items[0].volumeInfo.imageLinks.thumbnail);
            imageLink = response.data.items[0].volumeInfo.imageLinks.thumbnail;
            var settings2 = {
              headers: authenticationHeaders,
              method: 'PUT',
              //'X-Parse-Revocable-Session': 1,
              'Content-Type': 'application/json'
            };


            //console.log(dataObjectString);
            var data = {cover: imageLink};
            var dataObjectString = JSON.stringify(data);
            console.log(Item);
            var Url = 'https://api.parse.com/1/classes/Books/' + Item.objectId;
            return $http.put(Url, dataObjectString, settings2)
              .then(function (response) {
                // In the response resp.data contains the result
                // check the console to see all of the data returned
                console.log(response);
                return response.data;


                //response.data.items[0].volumeInfo.imageLinks
              }, function error(_errorResponse) {
                alert("ERROR " + _errorResponse);
              });

          })
      }
    }
  });
