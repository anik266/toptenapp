angular.module('app.directives', [])

.directive('blankDirective', [function(){

}])

.directive('xref',function($route, $location){
  return {
    link: function(scope, elm,attr){
      elm.on('click',function(){
        if ( $location.path() === attr.xref ) {
          $route.reload();
        } else {
          scope.$apply(function(){
            $location.path(attr.xref);
          });
        }
      });
    }
  };
});

//test comment
